#Imagen Base
FROM node:latest

#Directorio de la app en el contenedo
WORKDIR /app

#Copiado de archivos
ADD . /app

#Dependencias
RUN npm install

#Puerto que expongo
EXPOSE 3000

#Comandos de ejecución de la aplicación
CMD ["npm", "start"]